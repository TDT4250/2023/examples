/*
 * generated by Xtext 2.32.0
 */
package no.ntnu.tdt4250.sm.textual.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
public class StateMachineDSLUiModule extends AbstractStateMachineDSLUiModule {

	public StateMachineDSLUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
}
