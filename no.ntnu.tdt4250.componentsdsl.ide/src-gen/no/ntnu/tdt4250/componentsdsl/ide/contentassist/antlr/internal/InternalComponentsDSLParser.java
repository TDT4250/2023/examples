package no.ntnu.tdt4250.componentsdsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import no.ntnu.tdt4250.componentsdsl.services.ComponentsDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalComponentsDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Component'", "'provided'", "'required'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalComponentsDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalComponentsDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalComponentsDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalComponentsDSL.g"; }


    	private ComponentsDSLGrammarAccess grammarAccess;

    	public void setGrammarAccess(ComponentsDSLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleSystem"
    // InternalComponentsDSL.g:53:1: entryRuleSystem : ruleSystem EOF ;
    public final void entryRuleSystem() throws RecognitionException {
        try {
            // InternalComponentsDSL.g:54:1: ( ruleSystem EOF )
            // InternalComponentsDSL.g:55:1: ruleSystem EOF
            {
             before(grammarAccess.getSystemRule()); 
            pushFollow(FOLLOW_1);
            ruleSystem();

            state._fsp--;

             after(grammarAccess.getSystemRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSystem"


    // $ANTLR start "ruleSystem"
    // InternalComponentsDSL.g:62:1: ruleSystem : ( ( rule__System__ComponentsAssignment )* ) ;
    public final void ruleSystem() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:66:2: ( ( ( rule__System__ComponentsAssignment )* ) )
            // InternalComponentsDSL.g:67:2: ( ( rule__System__ComponentsAssignment )* )
            {
            // InternalComponentsDSL.g:67:2: ( ( rule__System__ComponentsAssignment )* )
            // InternalComponentsDSL.g:68:3: ( rule__System__ComponentsAssignment )*
            {
             before(grammarAccess.getSystemAccess().getComponentsAssignment()); 
            // InternalComponentsDSL.g:69:3: ( rule__System__ComponentsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalComponentsDSL.g:69:4: rule__System__ComponentsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__System__ComponentsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getSystemAccess().getComponentsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSystem"


    // $ANTLR start "entryRuleComponent"
    // InternalComponentsDSL.g:78:1: entryRuleComponent : ruleComponent EOF ;
    public final void entryRuleComponent() throws RecognitionException {
        try {
            // InternalComponentsDSL.g:79:1: ( ruleComponent EOF )
            // InternalComponentsDSL.g:80:1: ruleComponent EOF
            {
             before(grammarAccess.getComponentRule()); 
            pushFollow(FOLLOW_1);
            ruleComponent();

            state._fsp--;

             after(grammarAccess.getComponentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComponent"


    // $ANTLR start "ruleComponent"
    // InternalComponentsDSL.g:87:1: ruleComponent : ( ( rule__Component__Group__0 ) ) ;
    public final void ruleComponent() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:91:2: ( ( ( rule__Component__Group__0 ) ) )
            // InternalComponentsDSL.g:92:2: ( ( rule__Component__Group__0 ) )
            {
            // InternalComponentsDSL.g:92:2: ( ( rule__Component__Group__0 ) )
            // InternalComponentsDSL.g:93:3: ( rule__Component__Group__0 )
            {
             before(grammarAccess.getComponentAccess().getGroup()); 
            // InternalComponentsDSL.g:94:3: ( rule__Component__Group__0 )
            // InternalComponentsDSL.g:94:4: rule__Component__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Component__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComponent"


    // $ANTLR start "entryRulePort"
    // InternalComponentsDSL.g:103:1: entryRulePort : rulePort EOF ;
    public final void entryRulePort() throws RecognitionException {
        try {
            // InternalComponentsDSL.g:104:1: ( rulePort EOF )
            // InternalComponentsDSL.g:105:1: rulePort EOF
            {
             before(grammarAccess.getPortRule()); 
            pushFollow(FOLLOW_1);
            rulePort();

            state._fsp--;

             after(grammarAccess.getPortRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePort"


    // $ANTLR start "rulePort"
    // InternalComponentsDSL.g:112:1: rulePort : ( ( rule__Port__Group__0 ) ) ;
    public final void rulePort() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:116:2: ( ( ( rule__Port__Group__0 ) ) )
            // InternalComponentsDSL.g:117:2: ( ( rule__Port__Group__0 ) )
            {
            // InternalComponentsDSL.g:117:2: ( ( rule__Port__Group__0 ) )
            // InternalComponentsDSL.g:118:3: ( rule__Port__Group__0 )
            {
             before(grammarAccess.getPortAccess().getGroup()); 
            // InternalComponentsDSL.g:119:3: ( rule__Port__Group__0 )
            // InternalComponentsDSL.g:119:4: rule__Port__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Port__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPortAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePort"


    // $ANTLR start "rule__Component__Group__0"
    // InternalComponentsDSL.g:127:1: rule__Component__Group__0 : rule__Component__Group__0__Impl rule__Component__Group__1 ;
    public final void rule__Component__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:131:1: ( rule__Component__Group__0__Impl rule__Component__Group__1 )
            // InternalComponentsDSL.g:132:2: rule__Component__Group__0__Impl rule__Component__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Component__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Component__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__0"


    // $ANTLR start "rule__Component__Group__0__Impl"
    // InternalComponentsDSL.g:139:1: rule__Component__Group__0__Impl : ( 'Component' ) ;
    public final void rule__Component__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:143:1: ( ( 'Component' ) )
            // InternalComponentsDSL.g:144:1: ( 'Component' )
            {
            // InternalComponentsDSL.g:144:1: ( 'Component' )
            // InternalComponentsDSL.g:145:2: 'Component'
            {
             before(grammarAccess.getComponentAccess().getComponentKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getComponentAccess().getComponentKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__0__Impl"


    // $ANTLR start "rule__Component__Group__1"
    // InternalComponentsDSL.g:154:1: rule__Component__Group__1 : rule__Component__Group__1__Impl rule__Component__Group__2 ;
    public final void rule__Component__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:158:1: ( rule__Component__Group__1__Impl rule__Component__Group__2 )
            // InternalComponentsDSL.g:159:2: rule__Component__Group__1__Impl rule__Component__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Component__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Component__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__1"


    // $ANTLR start "rule__Component__Group__1__Impl"
    // InternalComponentsDSL.g:166:1: rule__Component__Group__1__Impl : ( ( rule__Component__NameAssignment_1 ) ) ;
    public final void rule__Component__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:170:1: ( ( ( rule__Component__NameAssignment_1 ) ) )
            // InternalComponentsDSL.g:171:1: ( ( rule__Component__NameAssignment_1 ) )
            {
            // InternalComponentsDSL.g:171:1: ( ( rule__Component__NameAssignment_1 ) )
            // InternalComponentsDSL.g:172:2: ( rule__Component__NameAssignment_1 )
            {
             before(grammarAccess.getComponentAccess().getNameAssignment_1()); 
            // InternalComponentsDSL.g:173:2: ( rule__Component__NameAssignment_1 )
            // InternalComponentsDSL.g:173:3: rule__Component__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Component__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__1__Impl"


    // $ANTLR start "rule__Component__Group__2"
    // InternalComponentsDSL.g:181:1: rule__Component__Group__2 : rule__Component__Group__2__Impl ;
    public final void rule__Component__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:185:1: ( rule__Component__Group__2__Impl )
            // InternalComponentsDSL.g:186:2: rule__Component__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Component__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__2"


    // $ANTLR start "rule__Component__Group__2__Impl"
    // InternalComponentsDSL.g:192:1: rule__Component__Group__2__Impl : ( ( ( rule__Component__PortsAssignment_2 ) ) ( ( rule__Component__PortsAssignment_2 )* ) ) ;
    public final void rule__Component__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:196:1: ( ( ( ( rule__Component__PortsAssignment_2 ) ) ( ( rule__Component__PortsAssignment_2 )* ) ) )
            // InternalComponentsDSL.g:197:1: ( ( ( rule__Component__PortsAssignment_2 ) ) ( ( rule__Component__PortsAssignment_2 )* ) )
            {
            // InternalComponentsDSL.g:197:1: ( ( ( rule__Component__PortsAssignment_2 ) ) ( ( rule__Component__PortsAssignment_2 )* ) )
            // InternalComponentsDSL.g:198:2: ( ( rule__Component__PortsAssignment_2 ) ) ( ( rule__Component__PortsAssignment_2 )* )
            {
            // InternalComponentsDSL.g:198:2: ( ( rule__Component__PortsAssignment_2 ) )
            // InternalComponentsDSL.g:199:3: ( rule__Component__PortsAssignment_2 )
            {
             before(grammarAccess.getComponentAccess().getPortsAssignment_2()); 
            // InternalComponentsDSL.g:200:3: ( rule__Component__PortsAssignment_2 )
            // InternalComponentsDSL.g:200:4: rule__Component__PortsAssignment_2
            {
            pushFollow(FOLLOW_5);
            rule__Component__PortsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getComponentAccess().getPortsAssignment_2()); 

            }

            // InternalComponentsDSL.g:203:2: ( ( rule__Component__PortsAssignment_2 )* )
            // InternalComponentsDSL.g:204:3: ( rule__Component__PortsAssignment_2 )*
            {
             before(grammarAccess.getComponentAccess().getPortsAssignment_2()); 
            // InternalComponentsDSL.g:205:3: ( rule__Component__PortsAssignment_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalComponentsDSL.g:205:4: rule__Component__PortsAssignment_2
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Component__PortsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getComponentAccess().getPortsAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__Group__2__Impl"


    // $ANTLR start "rule__Port__Group__0"
    // InternalComponentsDSL.g:215:1: rule__Port__Group__0 : rule__Port__Group__0__Impl rule__Port__Group__1 ;
    public final void rule__Port__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:219:1: ( rule__Port__Group__0__Impl rule__Port__Group__1 )
            // InternalComponentsDSL.g:220:2: rule__Port__Group__0__Impl rule__Port__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Port__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Port__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__Group__0"


    // $ANTLR start "rule__Port__Group__0__Impl"
    // InternalComponentsDSL.g:227:1: rule__Port__Group__0__Impl : ( ( rule__Port__NameAssignment_0 ) ) ;
    public final void rule__Port__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:231:1: ( ( ( rule__Port__NameAssignment_0 ) ) )
            // InternalComponentsDSL.g:232:1: ( ( rule__Port__NameAssignment_0 ) )
            {
            // InternalComponentsDSL.g:232:1: ( ( rule__Port__NameAssignment_0 ) )
            // InternalComponentsDSL.g:233:2: ( rule__Port__NameAssignment_0 )
            {
             before(grammarAccess.getPortAccess().getNameAssignment_0()); 
            // InternalComponentsDSL.g:234:2: ( rule__Port__NameAssignment_0 )
            // InternalComponentsDSL.g:234:3: rule__Port__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Port__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPortAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__Group__0__Impl"


    // $ANTLR start "rule__Port__Group__1"
    // InternalComponentsDSL.g:242:1: rule__Port__Group__1 : rule__Port__Group__1__Impl rule__Port__Group__2 ;
    public final void rule__Port__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:246:1: ( rule__Port__Group__1__Impl rule__Port__Group__2 )
            // InternalComponentsDSL.g:247:2: rule__Port__Group__1__Impl rule__Port__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Port__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Port__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__Group__1"


    // $ANTLR start "rule__Port__Group__1__Impl"
    // InternalComponentsDSL.g:254:1: rule__Port__Group__1__Impl : ( ( rule__Port__ProvidedAssignment_1 ) ) ;
    public final void rule__Port__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:258:1: ( ( ( rule__Port__ProvidedAssignment_1 ) ) )
            // InternalComponentsDSL.g:259:1: ( ( rule__Port__ProvidedAssignment_1 ) )
            {
            // InternalComponentsDSL.g:259:1: ( ( rule__Port__ProvidedAssignment_1 ) )
            // InternalComponentsDSL.g:260:2: ( rule__Port__ProvidedAssignment_1 )
            {
             before(grammarAccess.getPortAccess().getProvidedAssignment_1()); 
            // InternalComponentsDSL.g:261:2: ( rule__Port__ProvidedAssignment_1 )
            // InternalComponentsDSL.g:261:3: rule__Port__ProvidedAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Port__ProvidedAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPortAccess().getProvidedAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__Group__1__Impl"


    // $ANTLR start "rule__Port__Group__2"
    // InternalComponentsDSL.g:269:1: rule__Port__Group__2 : rule__Port__Group__2__Impl ;
    public final void rule__Port__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:273:1: ( rule__Port__Group__2__Impl )
            // InternalComponentsDSL.g:274:2: rule__Port__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Port__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__Group__2"


    // $ANTLR start "rule__Port__Group__2__Impl"
    // InternalComponentsDSL.g:280:1: rule__Port__Group__2__Impl : ( ( rule__Port__RequiredAssignment_2 ) ) ;
    public final void rule__Port__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:284:1: ( ( ( rule__Port__RequiredAssignment_2 ) ) )
            // InternalComponentsDSL.g:285:1: ( ( rule__Port__RequiredAssignment_2 ) )
            {
            // InternalComponentsDSL.g:285:1: ( ( rule__Port__RequiredAssignment_2 ) )
            // InternalComponentsDSL.g:286:2: ( rule__Port__RequiredAssignment_2 )
            {
             before(grammarAccess.getPortAccess().getRequiredAssignment_2()); 
            // InternalComponentsDSL.g:287:2: ( rule__Port__RequiredAssignment_2 )
            // InternalComponentsDSL.g:287:3: rule__Port__RequiredAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Port__RequiredAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPortAccess().getRequiredAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__Group__2__Impl"


    // $ANTLR start "rule__System__ComponentsAssignment"
    // InternalComponentsDSL.g:296:1: rule__System__ComponentsAssignment : ( ruleComponent ) ;
    public final void rule__System__ComponentsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:300:1: ( ( ruleComponent ) )
            // InternalComponentsDSL.g:301:2: ( ruleComponent )
            {
            // InternalComponentsDSL.g:301:2: ( ruleComponent )
            // InternalComponentsDSL.g:302:3: ruleComponent
            {
             before(grammarAccess.getSystemAccess().getComponentsComponentParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleComponent();

            state._fsp--;

             after(grammarAccess.getSystemAccess().getComponentsComponentParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__ComponentsAssignment"


    // $ANTLR start "rule__Component__NameAssignment_1"
    // InternalComponentsDSL.g:311:1: rule__Component__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Component__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:315:1: ( ( RULE_ID ) )
            // InternalComponentsDSL.g:316:2: ( RULE_ID )
            {
            // InternalComponentsDSL.g:316:2: ( RULE_ID )
            // InternalComponentsDSL.g:317:3: RULE_ID
            {
             before(grammarAccess.getComponentAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getComponentAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__NameAssignment_1"


    // $ANTLR start "rule__Component__PortsAssignment_2"
    // InternalComponentsDSL.g:326:1: rule__Component__PortsAssignment_2 : ( rulePort ) ;
    public final void rule__Component__PortsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:330:1: ( ( rulePort ) )
            // InternalComponentsDSL.g:331:2: ( rulePort )
            {
            // InternalComponentsDSL.g:331:2: ( rulePort )
            // InternalComponentsDSL.g:332:3: rulePort
            {
             before(grammarAccess.getComponentAccess().getPortsPortParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            rulePort();

            state._fsp--;

             after(grammarAccess.getComponentAccess().getPortsPortParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Component__PortsAssignment_2"


    // $ANTLR start "rule__Port__NameAssignment_0"
    // InternalComponentsDSL.g:341:1: rule__Port__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Port__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:345:1: ( ( RULE_ID ) )
            // InternalComponentsDSL.g:346:2: ( RULE_ID )
            {
            // InternalComponentsDSL.g:346:2: ( RULE_ID )
            // InternalComponentsDSL.g:347:3: RULE_ID
            {
             before(grammarAccess.getPortAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPortAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__NameAssignment_0"


    // $ANTLR start "rule__Port__ProvidedAssignment_1"
    // InternalComponentsDSL.g:356:1: rule__Port__ProvidedAssignment_1 : ( ( 'provided' ) ) ;
    public final void rule__Port__ProvidedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:360:1: ( ( ( 'provided' ) ) )
            // InternalComponentsDSL.g:361:2: ( ( 'provided' ) )
            {
            // InternalComponentsDSL.g:361:2: ( ( 'provided' ) )
            // InternalComponentsDSL.g:362:3: ( 'provided' )
            {
             before(grammarAccess.getPortAccess().getProvidedProvidedKeyword_1_0()); 
            // InternalComponentsDSL.g:363:3: ( 'provided' )
            // InternalComponentsDSL.g:364:4: 'provided'
            {
             before(grammarAccess.getPortAccess().getProvidedProvidedKeyword_1_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getPortAccess().getProvidedProvidedKeyword_1_0()); 

            }

             after(grammarAccess.getPortAccess().getProvidedProvidedKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__ProvidedAssignment_1"


    // $ANTLR start "rule__Port__RequiredAssignment_2"
    // InternalComponentsDSL.g:375:1: rule__Port__RequiredAssignment_2 : ( ( 'required' ) ) ;
    public final void rule__Port__RequiredAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalComponentsDSL.g:379:1: ( ( ( 'required' ) ) )
            // InternalComponentsDSL.g:380:2: ( ( 'required' ) )
            {
            // InternalComponentsDSL.g:380:2: ( ( 'required' ) )
            // InternalComponentsDSL.g:381:3: ( 'required' )
            {
             before(grammarAccess.getPortAccess().getRequiredRequiredKeyword_2_0()); 
            // InternalComponentsDSL.g:382:3: ( 'required' )
            // InternalComponentsDSL.g:383:4: 'required'
            {
             before(grammarAccess.getPortAccess().getRequiredRequiredKeyword_2_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getPortAccess().getRequiredRequiredKeyword_2_0()); 

            }

             after(grammarAccess.getPortAccess().getRequiredRequiredKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Port__RequiredAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});

}