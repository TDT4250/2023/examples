/*
 * generated by Xtext 2.32.0
 */
package no.ntnu.tdt4250.componentsdsl.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import no.ntnu.tdt4250.componentsdsl.ComponentsDSLRuntimeModule;
import no.ntnu.tdt4250.componentsdsl.ComponentsDSLStandaloneSetup;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
public class ComponentsDSLIdeSetup extends ComponentsDSLStandaloneSetup {

	@Override
	public Injector createInjector() {
		return Guice.createInjector(Modules2.mixin(new ComponentsDSLRuntimeModule(), new ComponentsDSLIdeModule()));
	}
	
}
