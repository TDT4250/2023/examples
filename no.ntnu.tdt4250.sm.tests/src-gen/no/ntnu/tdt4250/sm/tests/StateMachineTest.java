/**
 */
package no.ntnu.tdt4250.sm.tests;

import static org.junit.Assert.assertNotEquals;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import no.ntnu.tdt4250.sm.SmFactory;
import no.ntnu.tdt4250.sm.State;
import no.ntnu.tdt4250.sm.StateMachine;
import no.ntnu.tdt4250.sm.Transition;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#initialStatesShouldNotHaveIncomingTransitions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Initial States Should Not Have Incoming Transitions</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#finalStatesShouldNotHaveOutgoingTransitions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Final States Should Not Have Outgoing Transitions</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StateMachineTest extends TestCase {

	/**
	 * The fixture for this State Machine test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateMachine fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StateMachineTest.class);
	}

	/**
	 * Constructs a new State Machine test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachineTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this State Machine test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StateMachine fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this State Machine test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateMachine getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SmFactory.eINSTANCE.createStateMachine());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link no.ntnu.tdt4250.sm.StateMachine#initialStatesShouldNotHaveIncomingTransitions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Initial States Should Not Have Incoming Transitions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.ntnu.tdt4250.sm.StateMachine#initialStatesShouldNotHaveIncomingTransitions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testInitialStatesShouldNotHaveIncomingTransitions__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link no.ntnu.tdt4250.sm.StateMachine#finalStatesShouldNotHaveOutgoingTransitions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Final States Should Not Have Outgoing Transitions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.ntnu.tdt4250.sm.StateMachine#finalStatesShouldNotHaveOutgoingTransitions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated NOT
	 */
	public void testFinalStatesShouldNotHaveOutgoingTransitions__DiagnosticChain_Map() {
		// We use in this case the object returned by getFixture
		// We can configure that object in the method, or with setFixture.
		StateMachine machine = this.getFixture();
		// StateMachine machine = SmFactory.eINSTANCE.createStateMachine();
		State state1 = SmFactory.eINSTANCE.createState();
		State state2 = SmFactory.eINSTANCE.createState();
		Transition t1 = SmFactory.eINSTANCE.createTransition();

		machine.setName("SM");

		state1.setName("S1");
		state2.setName("S2");

		t1.setSource(state1);
		t1.setTarget(state2);
		t1.setName("T");

		machine.getState().add(state1);
		machine.getState().add(state2);
		machine.getTransition().add(t1);
		machine.getFinal().add(state2);

		// Constraints on a model are validated through the "Diagnostician" class.
		Diagnostic result = Diagnostician.INSTANCE.validate(machine);
		// Testing the positive case
		assertEquals(Diagnostic.OK, result.getSeverity());

		// We now test the negative case by loading an XMI file. This is just to show 
		// how it is done. Loading a file is probably more convenient than creating
		// model instances in code.
		ResourceSet rs = new ResourceSetImpl();
		Resource res = rs.getResource(
				URI.createFileURI("testcases/finalStatesShouldNotHaveOutgoingTransitions/shouldFail.xmi"), true);

		StateMachine invalidMachine = (StateMachine) res.getContents().get(0);

		Diagnostic resultNegative = Diagnostician.INSTANCE.validate(invalidMachine);
		assertNotEquals(Diagnostic.OK, resultNegative.getSeverity());
		// Testing constraints can be tricky, because you can get errors from other
		// constraints other than the one you are testing

		final String MSG_EXPECTED = "'StateMachine::finalStatesShouldNotHaveOutgoingTransitions' constraint is violated";
		for (Diagnostic child : resultNegative.getChildren()) {
			// We need therefore not only to check the result of validation,
			// but also to check that the only error message that we get is the one
			// that we expected, and not something else.
			assertTrue(child.getMessage().contains(MSG_EXPECTED));

			System.out.println(child.toString());
		}
	}

} //StateMachineTest
