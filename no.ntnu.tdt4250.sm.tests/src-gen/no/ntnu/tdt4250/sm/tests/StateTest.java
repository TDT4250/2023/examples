/**
 */
package no.ntnu.tdt4250.sm.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import no.ntnu.tdt4250.sm.SmFactory;
import no.ntnu.tdt4250.sm.State;
import no.ntnu.tdt4250.sm.Transition;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.sm.State#isInitial() <em>Initial</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#isHasLoops() <em>Has Loops</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StateTest extends TestCase {

	/**
	 * The fixture for this State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected State fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StateTest.class);
	}

	/**
	 * Constructs a new State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(State fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected State getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SmFactory.eINSTANCE.createState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link no.ntnu.tdt4250.sm.State#isInitial() <em>Initial</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.ntnu.tdt4250.sm.State#isInitial()
	 * @generated
	 */
	public void testIsInitial() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link no.ntnu.tdt4250.sm.State#isHasLoops() <em>Has Loops</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.ntnu.tdt4250.sm.State#isHasLoops()
	 * @generated NOT
	 */
	public void testIsHasLoops() {
		// The EMF pattern is to add "is" for Boolean features, hence the
		// strange name of this method. Perhaps the property could be called 
		// "withLoops" instead (thus resulting in "isWithLoops")

		// For the state we could use the getFixture() method in the
		// same class. That is simply a method to use when you always want
		// to have access to the same object during testing.
		// Here is available for State, because we are in the StateTest class.
		State state = SmFactory.eINSTANCE.createState();
		// for the Transition, here we need to instead use the factory anyways
		Transition t1 = SmFactory.eINSTANCE.createTransition();

		t1.setSource(state);
		t1.setTarget(state);

		// Testing the positive case
		assertEquals(true, state.isHasLoops());

		State state2 = SmFactory.eINSTANCE.createState();

		t1.setSource(state);
		t1.setTarget(state2);

		// Testing the negative case 
		// More complex testing configurations can be defined, of course
		assertEquals(false, state.isHasLoops());
	}

} //StateTest
