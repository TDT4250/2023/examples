/*
 * generated by Xtext 2.32.0
 */
package no.ntnu.tdt4250.sm.textual.formatting2

import com.google.inject.Inject
import no.ntnu.tdt4250.sm.State
import no.ntnu.tdt4250.sm.StateMachine
import no.ntnu.tdt4250.sm.textual.services.StateMachineDSLGrammarAccess
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument

class StateMachineDSLFormatter extends AbstractFormatter2 {
	
	@Inject extension StateMachineDSLGrammarAccess

	def dispatch void format(StateMachine stateMachine, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (state : stateMachine.state) {
			state.format
		}
		for (transition : stateMachine.transition) {
			transition.format
		}
	}

	def dispatch void format(State state, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (_state : state.children) {
			_state.format
		}
	}
	
}
