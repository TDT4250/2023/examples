/*
 * generated by Xtext 2.32.0
 */
package no.ntnu.tdt4250.sm.textual;


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class StateMachineDSLRuntimeModule extends AbstractStateMachineDSLRuntimeModule {
}
