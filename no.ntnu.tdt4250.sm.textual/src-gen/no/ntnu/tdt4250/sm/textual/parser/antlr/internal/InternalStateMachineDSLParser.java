package no.ntnu.tdt4250.sm.textual.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import no.ntnu.tdt4250.sm.textual.services.StateMachineDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalStateMachineDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'StateMachine'", "'states'", "'{'", "'}'", "'transitions'", "'initial'", "'final'", "'('", "','", "')'", "':'", "'-->'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalStateMachineDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalStateMachineDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalStateMachineDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalStateMachineDSL.g"; }



     	private StateMachineDSLGrammarAccess grammarAccess;

        public InternalStateMachineDSLParser(TokenStream input, StateMachineDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "SM";
       	}

       	@Override
       	protected StateMachineDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSM"
    // InternalStateMachineDSL.g:64:1: entryRuleSM returns [EObject current=null] : iv_ruleSM= ruleSM EOF ;
    public final EObject entryRuleSM() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSM = null;


        try {
            // InternalStateMachineDSL.g:64:43: (iv_ruleSM= ruleSM EOF )
            // InternalStateMachineDSL.g:65:2: iv_ruleSM= ruleSM EOF
            {
             newCompositeNode(grammarAccess.getSMRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSM=ruleSM();

            state._fsp--;

             current =iv_ruleSM; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSM"


    // $ANTLR start "ruleSM"
    // InternalStateMachineDSL.g:71:1: ruleSM returns [EObject current=null] : (otherlv_0= 'StateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'states' otherlv_3= '{' ( (lv_state_4_0= ruleState ) ) ( (lv_state_5_0= ruleState ) )* otherlv_6= '}' otherlv_7= 'transitions' otherlv_8= '{' ( (lv_transition_9_0= ruleTransition ) ) ( (lv_transition_10_0= ruleTransition ) )* otherlv_11= '}' otherlv_12= 'initial' ( ( ruleEString ) ) otherlv_14= 'final' otherlv_15= '(' ( ( ruleEString ) ) (otherlv_17= ',' ( ( ruleEString ) ) )* otherlv_19= ')' ) ;
    public final EObject ruleSM() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_state_4_0 = null;

        EObject lv_state_5_0 = null;

        EObject lv_transition_9_0 = null;

        EObject lv_transition_10_0 = null;



        	enterRule();

        try {
            // InternalStateMachineDSL.g:77:2: ( (otherlv_0= 'StateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'states' otherlv_3= '{' ( (lv_state_4_0= ruleState ) ) ( (lv_state_5_0= ruleState ) )* otherlv_6= '}' otherlv_7= 'transitions' otherlv_8= '{' ( (lv_transition_9_0= ruleTransition ) ) ( (lv_transition_10_0= ruleTransition ) )* otherlv_11= '}' otherlv_12= 'initial' ( ( ruleEString ) ) otherlv_14= 'final' otherlv_15= '(' ( ( ruleEString ) ) (otherlv_17= ',' ( ( ruleEString ) ) )* otherlv_19= ')' ) )
            // InternalStateMachineDSL.g:78:2: (otherlv_0= 'StateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'states' otherlv_3= '{' ( (lv_state_4_0= ruleState ) ) ( (lv_state_5_0= ruleState ) )* otherlv_6= '}' otherlv_7= 'transitions' otherlv_8= '{' ( (lv_transition_9_0= ruleTransition ) ) ( (lv_transition_10_0= ruleTransition ) )* otherlv_11= '}' otherlv_12= 'initial' ( ( ruleEString ) ) otherlv_14= 'final' otherlv_15= '(' ( ( ruleEString ) ) (otherlv_17= ',' ( ( ruleEString ) ) )* otherlv_19= ')' )
            {
            // InternalStateMachineDSL.g:78:2: (otherlv_0= 'StateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'states' otherlv_3= '{' ( (lv_state_4_0= ruleState ) ) ( (lv_state_5_0= ruleState ) )* otherlv_6= '}' otherlv_7= 'transitions' otherlv_8= '{' ( (lv_transition_9_0= ruleTransition ) ) ( (lv_transition_10_0= ruleTransition ) )* otherlv_11= '}' otherlv_12= 'initial' ( ( ruleEString ) ) otherlv_14= 'final' otherlv_15= '(' ( ( ruleEString ) ) (otherlv_17= ',' ( ( ruleEString ) ) )* otherlv_19= ')' )
            // InternalStateMachineDSL.g:79:3: otherlv_0= 'StateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'states' otherlv_3= '{' ( (lv_state_4_0= ruleState ) ) ( (lv_state_5_0= ruleState ) )* otherlv_6= '}' otherlv_7= 'transitions' otherlv_8= '{' ( (lv_transition_9_0= ruleTransition ) ) ( (lv_transition_10_0= ruleTransition ) )* otherlv_11= '}' otherlv_12= 'initial' ( ( ruleEString ) ) otherlv_14= 'final' otherlv_15= '(' ( ( ruleEString ) ) (otherlv_17= ',' ( ( ruleEString ) ) )* otherlv_19= ')'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getSMAccess().getStateMachineKeyword_0());
            		
            // InternalStateMachineDSL.g:83:3: ( (lv_name_1_0= ruleEString ) )
            // InternalStateMachineDSL.g:84:4: (lv_name_1_0= ruleEString )
            {
            // InternalStateMachineDSL.g:84:4: (lv_name_1_0= ruleEString )
            // InternalStateMachineDSL.g:85:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSMAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSMRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"no.ntnu.tdt4250.sm.textual.StateMachineDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getSMAccess().getStatesKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getSMAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalStateMachineDSL.g:110:3: ( (lv_state_4_0= ruleState ) )
            // InternalStateMachineDSL.g:111:4: (lv_state_4_0= ruleState )
            {
            // InternalStateMachineDSL.g:111:4: (lv_state_4_0= ruleState )
            // InternalStateMachineDSL.g:112:5: lv_state_4_0= ruleState
            {

            					newCompositeNode(grammarAccess.getSMAccess().getStateStateParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_6);
            lv_state_4_0=ruleState();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSMRule());
            					}
            					add(
            						current,
            						"state",
            						lv_state_4_0,
            						"no.ntnu.tdt4250.sm.textual.StateMachineDSL.State");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalStateMachineDSL.g:129:3: ( (lv_state_5_0= ruleState ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_STRING && LA1_0<=RULE_ID)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalStateMachineDSL.g:130:4: (lv_state_5_0= ruleState )
            	    {
            	    // InternalStateMachineDSL.g:130:4: (lv_state_5_0= ruleState )
            	    // InternalStateMachineDSL.g:131:5: lv_state_5_0= ruleState
            	    {

            	    					newCompositeNode(grammarAccess.getSMAccess().getStateStateParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_state_5_0=ruleState();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSMRule());
            	    					}
            	    					add(
            	    						current,
            	    						"state",
            	    						lv_state_5_0,
            	    						"no.ntnu.tdt4250.sm.textual.StateMachineDSL.State");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_6=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_6, grammarAccess.getSMAccess().getRightCurlyBracketKeyword_6());
            		
            otherlv_7=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_7, grammarAccess.getSMAccess().getTransitionsKeyword_7());
            		
            otherlv_8=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_8, grammarAccess.getSMAccess().getLeftCurlyBracketKeyword_8());
            		
            // InternalStateMachineDSL.g:160:3: ( (lv_transition_9_0= ruleTransition ) )
            // InternalStateMachineDSL.g:161:4: (lv_transition_9_0= ruleTransition )
            {
            // InternalStateMachineDSL.g:161:4: (lv_transition_9_0= ruleTransition )
            // InternalStateMachineDSL.g:162:5: lv_transition_9_0= ruleTransition
            {

            					newCompositeNode(grammarAccess.getSMAccess().getTransitionTransitionParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_6);
            lv_transition_9_0=ruleTransition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSMRule());
            					}
            					add(
            						current,
            						"transition",
            						lv_transition_9_0,
            						"no.ntnu.tdt4250.sm.textual.StateMachineDSL.Transition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalStateMachineDSL.g:179:3: ( (lv_transition_10_0= ruleTransition ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=RULE_STRING && LA2_0<=RULE_ID)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalStateMachineDSL.g:180:4: (lv_transition_10_0= ruleTransition )
            	    {
            	    // InternalStateMachineDSL.g:180:4: (lv_transition_10_0= ruleTransition )
            	    // InternalStateMachineDSL.g:181:5: lv_transition_10_0= ruleTransition
            	    {

            	    					newCompositeNode(grammarAccess.getSMAccess().getTransitionTransitionParserRuleCall_10_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_transition_10_0=ruleTransition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSMRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transition",
            	    						lv_transition_10_0,
            	    						"no.ntnu.tdt4250.sm.textual.StateMachineDSL.Transition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_11=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_11, grammarAccess.getSMAccess().getRightCurlyBracketKeyword_11());
            		
            otherlv_12=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_12, grammarAccess.getSMAccess().getInitialKeyword_12());
            		
            // InternalStateMachineDSL.g:206:3: ( ( ruleEString ) )
            // InternalStateMachineDSL.g:207:4: ( ruleEString )
            {
            // InternalStateMachineDSL.g:207:4: ( ruleEString )
            // InternalStateMachineDSL.g:208:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSMRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSMAccess().getInitialStateCrossReference_13_0());
            				
            pushFollow(FOLLOW_9);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_14=(Token)match(input,17,FOLLOW_10); 

            			newLeafNode(otherlv_14, grammarAccess.getSMAccess().getFinalKeyword_14());
            		
            otherlv_15=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_15, grammarAccess.getSMAccess().getLeftParenthesisKeyword_15());
            		
            // InternalStateMachineDSL.g:230:3: ( ( ruleEString ) )
            // InternalStateMachineDSL.g:231:4: ( ruleEString )
            {
            // InternalStateMachineDSL.g:231:4: ( ruleEString )
            // InternalStateMachineDSL.g:232:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSMRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSMAccess().getFinalStateCrossReference_16_0());
            				
            pushFollow(FOLLOW_11);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalStateMachineDSL.g:246:3: (otherlv_17= ',' ( ( ruleEString ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==19) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalStateMachineDSL.g:247:4: otherlv_17= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_17=(Token)match(input,19,FOLLOW_3); 

            	    				newLeafNode(otherlv_17, grammarAccess.getSMAccess().getCommaKeyword_17_0());
            	    			
            	    // InternalStateMachineDSL.g:251:4: ( ( ruleEString ) )
            	    // InternalStateMachineDSL.g:252:5: ( ruleEString )
            	    {
            	    // InternalStateMachineDSL.g:252:5: ( ruleEString )
            	    // InternalStateMachineDSL.g:253:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getSMRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getSMAccess().getFinalStateCrossReference_17_1_0());
            	    					
            	    pushFollow(FOLLOW_11);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_19=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_19, grammarAccess.getSMAccess().getRightParenthesisKeyword_18());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSM"


    // $ANTLR start "entryRuleState"
    // InternalStateMachineDSL.g:276:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalStateMachineDSL.g:276:46: (iv_ruleState= ruleState EOF )
            // InternalStateMachineDSL.g:277:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalStateMachineDSL.g:283:1: ruleState returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '{' ( (lv_children_2_0= ruleState ) ) (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )* otherlv_5= '}' )? ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_children_2_0 = null;

        EObject lv_children_4_0 = null;



        	enterRule();

        try {
            // InternalStateMachineDSL.g:289:2: ( ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '{' ( (lv_children_2_0= ruleState ) ) (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )* otherlv_5= '}' )? ) )
            // InternalStateMachineDSL.g:290:2: ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '{' ( (lv_children_2_0= ruleState ) ) (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )* otherlv_5= '}' )? )
            {
            // InternalStateMachineDSL.g:290:2: ( ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '{' ( (lv_children_2_0= ruleState ) ) (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )* otherlv_5= '}' )? )
            // InternalStateMachineDSL.g:291:3: ( (lv_name_0_0= ruleEString ) ) (otherlv_1= '{' ( (lv_children_2_0= ruleState ) ) (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )* otherlv_5= '}' )?
            {
            // InternalStateMachineDSL.g:291:3: ( (lv_name_0_0= ruleEString ) )
            // InternalStateMachineDSL.g:292:4: (lv_name_0_0= ruleEString )
            {
            // InternalStateMachineDSL.g:292:4: (lv_name_0_0= ruleEString )
            // InternalStateMachineDSL.g:293:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStateAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_12);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"no.ntnu.tdt4250.sm.textual.StateMachineDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalStateMachineDSL.g:310:3: (otherlv_1= '{' ( (lv_children_2_0= ruleState ) ) (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )* otherlv_5= '}' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==13) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalStateMachineDSL.g:311:4: otherlv_1= '{' ( (lv_children_2_0= ruleState ) ) (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )* otherlv_5= '}'
                    {
                    otherlv_1=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_1, grammarAccess.getStateAccess().getLeftCurlyBracketKeyword_1_0());
                    			
                    // InternalStateMachineDSL.g:315:4: ( (lv_children_2_0= ruleState ) )
                    // InternalStateMachineDSL.g:316:5: (lv_children_2_0= ruleState )
                    {
                    // InternalStateMachineDSL.g:316:5: (lv_children_2_0= ruleState )
                    // InternalStateMachineDSL.g:317:6: lv_children_2_0= ruleState
                    {

                    						newCompositeNode(grammarAccess.getStateAccess().getChildrenStateParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_13);
                    lv_children_2_0=ruleState();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStateRule());
                    						}
                    						add(
                    							current,
                    							"children",
                    							lv_children_2_0,
                    							"no.ntnu.tdt4250.sm.textual.StateMachineDSL.State");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalStateMachineDSL.g:334:4: (otherlv_3= ',' ( (lv_children_4_0= ruleState ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==19) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalStateMachineDSL.g:335:5: otherlv_3= ',' ( (lv_children_4_0= ruleState ) )
                    	    {
                    	    otherlv_3=(Token)match(input,19,FOLLOW_3); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getStateAccess().getCommaKeyword_1_2_0());
                    	    				
                    	    // InternalStateMachineDSL.g:339:5: ( (lv_children_4_0= ruleState ) )
                    	    // InternalStateMachineDSL.g:340:6: (lv_children_4_0= ruleState )
                    	    {
                    	    // InternalStateMachineDSL.g:340:6: (lv_children_4_0= ruleState )
                    	    // InternalStateMachineDSL.g:341:7: lv_children_4_0= ruleState
                    	    {

                    	    							newCompositeNode(grammarAccess.getStateAccess().getChildrenStateParserRuleCall_1_2_1_0());
                    	    						
                    	    pushFollow(FOLLOW_13);
                    	    lv_children_4_0=ruleState();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getStateRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"children",
                    	    								lv_children_4_0,
                    	    								"no.ntnu.tdt4250.sm.textual.StateMachineDSL.State");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_5, grammarAccess.getStateAccess().getRightCurlyBracketKeyword_1_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalStateMachineDSL.g:368:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalStateMachineDSL.g:368:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalStateMachineDSL.g:369:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalStateMachineDSL.g:375:1: ruleTransition returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) otherlv_3= '-->' ( ( ruleEString ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalStateMachineDSL.g:381:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) otherlv_3= '-->' ( ( ruleEString ) ) ) )
            // InternalStateMachineDSL.g:382:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) otherlv_3= '-->' ( ( ruleEString ) ) )
            {
            // InternalStateMachineDSL.g:382:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) otherlv_3= '-->' ( ( ruleEString ) ) )
            // InternalStateMachineDSL.g:383:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) otherlv_3= '-->' ( ( ruleEString ) )
            {
            // InternalStateMachineDSL.g:383:3: ( (lv_name_0_0= ruleEString ) )
            // InternalStateMachineDSL.g:384:4: (lv_name_0_0= ruleEString )
            {
            // InternalStateMachineDSL.g:384:4: (lv_name_0_0= ruleEString )
            // InternalStateMachineDSL.g:385:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTransitionAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_14);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"no.ntnu.tdt4250.sm.textual.StateMachineDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getColonKeyword_1());
            		
            // InternalStateMachineDSL.g:406:3: ( ( ruleEString ) )
            // InternalStateMachineDSL.g:407:4: ( ruleEString )
            {
            // InternalStateMachineDSL.g:407:4: ( ruleEString )
            // InternalStateMachineDSL.g:408:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getSourceStateCrossReference_2_0());
            				
            pushFollow(FOLLOW_15);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getHyphenMinusHyphenMinusGreaterThanSignKeyword_3());
            		
            // InternalStateMachineDSL.g:426:3: ( ( ruleEString ) )
            // InternalStateMachineDSL.g:427:4: ( ruleEString )
            {
            // InternalStateMachineDSL.g:427:4: ( ruleEString )
            // InternalStateMachineDSL.g:428:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getTargetStateCrossReference_4_0());
            				
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleEString"
    // InternalStateMachineDSL.g:446:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalStateMachineDSL.g:446:47: (iv_ruleEString= ruleEString EOF )
            // InternalStateMachineDSL.g:447:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalStateMachineDSL.g:453:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalStateMachineDSL.g:459:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalStateMachineDSL.g:460:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalStateMachineDSL.g:460:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_STRING) ) {
                alt6=1;
            }
            else if ( (LA6_0==RULE_ID) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalStateMachineDSL.g:461:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalStateMachineDSL.g:469:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000084000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400000L});

}