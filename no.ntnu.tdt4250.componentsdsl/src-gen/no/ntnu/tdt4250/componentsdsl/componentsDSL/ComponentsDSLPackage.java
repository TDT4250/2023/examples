/**
 * generated by Xtext 2.32.0
 */
package no.ntnu.tdt4250.componentsdsl.componentsDSL;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.ComponentsDSLFactory
 * @model kind="package"
 * @generated
 */
public interface ComponentsDSLPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "componentsDSL";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.ntnu.no/tdt4250/componentsdsl/ComponentsDSL";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "componentsDSL";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ComponentsDSLPackage eINSTANCE = no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentsDSLPackageImpl.init();

  /**
   * The meta object id for the '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.SystemImpl <em>System</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.SystemImpl
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentsDSLPackageImpl#getSystem()
   * @generated
   */
  int SYSTEM = 0;

  /**
   * The feature id for the '<em><b>Components</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYSTEM__COMPONENTS = 0;

  /**
   * The number of structural features of the '<em>System</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYSTEM_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentImpl <em>Component</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentImpl
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentsDSLPackageImpl#getComponent()
   * @generated
   */
  int COMPONENT = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT__NAME = 0;

  /**
   * The feature id for the '<em><b>Ports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT__PORTS = 1;

  /**
   * The number of structural features of the '<em>Component</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPONENT_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.PortImpl <em>Port</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.PortImpl
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentsDSLPackageImpl#getPort()
   * @generated
   */
  int PORT = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT__NAME = 0;

  /**
   * The feature id for the '<em><b>Provided</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT__PROVIDED = 1;

  /**
   * The feature id for the '<em><b>Required</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT__REQUIRED = 2;

  /**
   * The number of structural features of the '<em>Port</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PORT_FEATURE_COUNT = 3;


  /**
   * Returns the meta object for class '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.System <em>System</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>System</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.System
   * @generated
   */
  EClass getSystem();

  /**
   * Returns the meta object for the containment reference list '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.System#getComponents <em>Components</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Components</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.System#getComponents()
   * @see #getSystem()
   * @generated
   */
  EReference getSystem_Components();

  /**
   * Returns the meta object for class '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.Component <em>Component</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Component</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.Component
   * @generated
   */
  EClass getComponent();

  /**
   * Returns the meta object for the attribute '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.Component#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.Component#getName()
   * @see #getComponent()
   * @generated
   */
  EAttribute getComponent_Name();

  /**
   * Returns the meta object for the containment reference list '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.Component#getPorts <em>Ports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ports</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.Component#getPorts()
   * @see #getComponent()
   * @generated
   */
  EReference getComponent_Ports();

  /**
   * Returns the meta object for class '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.Port <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Port</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.Port
   * @generated
   */
  EClass getPort();

  /**
   * Returns the meta object for the attribute '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.Port#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.Port#getName()
   * @see #getPort()
   * @generated
   */
  EAttribute getPort_Name();

  /**
   * Returns the meta object for the attribute '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.Port#isProvided <em>Provided</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Provided</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.Port#isProvided()
   * @see #getPort()
   * @generated
   */
  EAttribute getPort_Provided();

  /**
   * Returns the meta object for the attribute '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.Port#isRequired <em>Required</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Required</em>'.
   * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.Port#isRequired()
   * @see #getPort()
   * @generated
   */
  EAttribute getPort_Required();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  ComponentsDSLFactory getComponentsDSLFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.SystemImpl <em>System</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.SystemImpl
     * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentsDSLPackageImpl#getSystem()
     * @generated
     */
    EClass SYSTEM = eINSTANCE.getSystem();

    /**
     * The meta object literal for the '<em><b>Components</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYSTEM__COMPONENTS = eINSTANCE.getSystem_Components();

    /**
     * The meta object literal for the '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentImpl <em>Component</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentImpl
     * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentsDSLPackageImpl#getComponent()
     * @generated
     */
    EClass COMPONENT = eINSTANCE.getComponent();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMPONENT__NAME = eINSTANCE.getComponent_Name();

    /**
     * The meta object literal for the '<em><b>Ports</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPONENT__PORTS = eINSTANCE.getComponent_Ports();

    /**
     * The meta object literal for the '{@link no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.PortImpl <em>Port</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.PortImpl
     * @see no.ntnu.tdt4250.componentsdsl.componentsDSL.impl.ComponentsDSLPackageImpl#getPort()
     * @generated
     */
    EClass PORT = eINSTANCE.getPort();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PORT__NAME = eINSTANCE.getPort_Name();

    /**
     * The meta object literal for the '<em><b>Provided</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PORT__PROVIDED = eINSTANCE.getPort_Provided();

    /**
     * The meta object literal for the '<em><b>Required</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PORT__REQUIRED = eINSTANCE.getPort_Required();

  }

} //ComponentsDSLPackage
