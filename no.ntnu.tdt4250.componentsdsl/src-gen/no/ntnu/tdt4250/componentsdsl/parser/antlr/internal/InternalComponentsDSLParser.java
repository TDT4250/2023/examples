package no.ntnu.tdt4250.componentsdsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import no.ntnu.tdt4250.componentsdsl.services.ComponentsDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalComponentsDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Component'", "'provided'", "'required'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=5;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalComponentsDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalComponentsDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalComponentsDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalComponentsDSL.g"; }



     	private ComponentsDSLGrammarAccess grammarAccess;

        public InternalComponentsDSLParser(TokenStream input, ComponentsDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "System";
       	}

       	@Override
       	protected ComponentsDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSystem"
    // InternalComponentsDSL.g:64:1: entryRuleSystem returns [EObject current=null] : iv_ruleSystem= ruleSystem EOF ;
    public final EObject entryRuleSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSystem = null;


        try {
            // InternalComponentsDSL.g:64:47: (iv_ruleSystem= ruleSystem EOF )
            // InternalComponentsDSL.g:65:2: iv_ruleSystem= ruleSystem EOF
            {
             newCompositeNode(grammarAccess.getSystemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSystem=ruleSystem();

            state._fsp--;

             current =iv_ruleSystem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSystem"


    // $ANTLR start "ruleSystem"
    // InternalComponentsDSL.g:71:1: ruleSystem returns [EObject current=null] : ( (lv_components_0_0= ruleComponent ) )* ;
    public final EObject ruleSystem() throws RecognitionException {
        EObject current = null;

        EObject lv_components_0_0 = null;



        	enterRule();

        try {
            // InternalComponentsDSL.g:77:2: ( ( (lv_components_0_0= ruleComponent ) )* )
            // InternalComponentsDSL.g:78:2: ( (lv_components_0_0= ruleComponent ) )*
            {
            // InternalComponentsDSL.g:78:2: ( (lv_components_0_0= ruleComponent ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalComponentsDSL.g:79:3: (lv_components_0_0= ruleComponent )
            	    {
            	    // InternalComponentsDSL.g:79:3: (lv_components_0_0= ruleComponent )
            	    // InternalComponentsDSL.g:80:4: lv_components_0_0= ruleComponent
            	    {

            	    				newCompositeNode(grammarAccess.getSystemAccess().getComponentsComponentParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_components_0_0=ruleComponent();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getSystemRule());
            	    				}
            	    				add(
            	    					current,
            	    					"components",
            	    					lv_components_0_0,
            	    					"no.ntnu.tdt4250.componentsdsl.ComponentsDSL.Component");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSystem"


    // $ANTLR start "entryRuleComponent"
    // InternalComponentsDSL.g:100:1: entryRuleComponent returns [EObject current=null] : iv_ruleComponent= ruleComponent EOF ;
    public final EObject entryRuleComponent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponent = null;


        try {
            // InternalComponentsDSL.g:100:50: (iv_ruleComponent= ruleComponent EOF )
            // InternalComponentsDSL.g:101:2: iv_ruleComponent= ruleComponent EOF
            {
             newCompositeNode(grammarAccess.getComponentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComponent=ruleComponent();

            state._fsp--;

             current =iv_ruleComponent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponent"


    // $ANTLR start "ruleComponent"
    // InternalComponentsDSL.g:107:1: ruleComponent returns [EObject current=null] : (otherlv_0= 'Component' ( (lv_name_1_0= RULE_ID ) ) ( (lv_ports_2_0= rulePort ) )+ ) ;
    public final EObject ruleComponent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_ports_2_0 = null;



        	enterRule();

        try {
            // InternalComponentsDSL.g:113:2: ( (otherlv_0= 'Component' ( (lv_name_1_0= RULE_ID ) ) ( (lv_ports_2_0= rulePort ) )+ ) )
            // InternalComponentsDSL.g:114:2: (otherlv_0= 'Component' ( (lv_name_1_0= RULE_ID ) ) ( (lv_ports_2_0= rulePort ) )+ )
            {
            // InternalComponentsDSL.g:114:2: (otherlv_0= 'Component' ( (lv_name_1_0= RULE_ID ) ) ( (lv_ports_2_0= rulePort ) )+ )
            // InternalComponentsDSL.g:115:3: otherlv_0= 'Component' ( (lv_name_1_0= RULE_ID ) ) ( (lv_ports_2_0= rulePort ) )+
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getComponentAccess().getComponentKeyword_0());
            		
            // InternalComponentsDSL.g:119:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalComponentsDSL.g:120:4: (lv_name_1_0= RULE_ID )
            {
            // InternalComponentsDSL.g:120:4: (lv_name_1_0= RULE_ID )
            // InternalComponentsDSL.g:121:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getComponentAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComponentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalComponentsDSL.g:137:3: ( (lv_ports_2_0= rulePort ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalComponentsDSL.g:138:4: (lv_ports_2_0= rulePort )
            	    {
            	    // InternalComponentsDSL.g:138:4: (lv_ports_2_0= rulePort )
            	    // InternalComponentsDSL.g:139:5: lv_ports_2_0= rulePort
            	    {

            	    					newCompositeNode(grammarAccess.getComponentAccess().getPortsPortParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_ports_2_0=rulePort();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComponentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"ports",
            	    						lv_ports_2_0,
            	    						"no.ntnu.tdt4250.componentsdsl.ComponentsDSL.Port");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponent"


    // $ANTLR start "entryRulePort"
    // InternalComponentsDSL.g:160:1: entryRulePort returns [EObject current=null] : iv_rulePort= rulePort EOF ;
    public final EObject entryRulePort() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePort = null;


        try {
            // InternalComponentsDSL.g:160:45: (iv_rulePort= rulePort EOF )
            // InternalComponentsDSL.g:161:2: iv_rulePort= rulePort EOF
            {
             newCompositeNode(grammarAccess.getPortRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePort=rulePort();

            state._fsp--;

             current =iv_rulePort; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePort"


    // $ANTLR start "rulePort"
    // InternalComponentsDSL.g:167:1: rulePort returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_provided_1_0= 'provided' ) ) ( (lv_required_2_0= 'required' ) ) ) ;
    public final EObject rulePort() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token lv_provided_1_0=null;
        Token lv_required_2_0=null;


        	enterRule();

        try {
            // InternalComponentsDSL.g:173:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_provided_1_0= 'provided' ) ) ( (lv_required_2_0= 'required' ) ) ) )
            // InternalComponentsDSL.g:174:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_provided_1_0= 'provided' ) ) ( (lv_required_2_0= 'required' ) ) )
            {
            // InternalComponentsDSL.g:174:2: ( ( (lv_name_0_0= RULE_ID ) ) ( (lv_provided_1_0= 'provided' ) ) ( (lv_required_2_0= 'required' ) ) )
            // InternalComponentsDSL.g:175:3: ( (lv_name_0_0= RULE_ID ) ) ( (lv_provided_1_0= 'provided' ) ) ( (lv_required_2_0= 'required' ) )
            {
            // InternalComponentsDSL.g:175:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalComponentsDSL.g:176:4: (lv_name_0_0= RULE_ID )
            {
            // InternalComponentsDSL.g:176:4: (lv_name_0_0= RULE_ID )
            // InternalComponentsDSL.g:177:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_0_0, grammarAccess.getPortAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPortRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalComponentsDSL.g:193:3: ( (lv_provided_1_0= 'provided' ) )
            // InternalComponentsDSL.g:194:4: (lv_provided_1_0= 'provided' )
            {
            // InternalComponentsDSL.g:194:4: (lv_provided_1_0= 'provided' )
            // InternalComponentsDSL.g:195:5: lv_provided_1_0= 'provided'
            {
            lv_provided_1_0=(Token)match(input,12,FOLLOW_7); 

            					newLeafNode(lv_provided_1_0, grammarAccess.getPortAccess().getProvidedProvidedKeyword_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPortRule());
            					}
            					setWithLastConsumed(current, "provided", lv_provided_1_0 != null, "provided");
            				

            }


            }

            // InternalComponentsDSL.g:207:3: ( (lv_required_2_0= 'required' ) )
            // InternalComponentsDSL.g:208:4: (lv_required_2_0= 'required' )
            {
            // InternalComponentsDSL.g:208:4: (lv_required_2_0= 'required' )
            // InternalComponentsDSL.g:209:5: lv_required_2_0= 'required'
            {
            lv_required_2_0=(Token)match(input,13,FOLLOW_2); 

            					newLeafNode(lv_required_2_0, grammarAccess.getPortAccess().getRequiredRequiredKeyword_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPortRule());
            					}
            					setWithLastConsumed(current, "required", lv_required_2_0 != null, "required");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePort"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});

}