package no.ntnu.tdt4250.sm.util;

import java.io.IOException;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import no.ntnu.tdt4250.sm.SmFactory;
import no.ntnu.tdt4250.sm.SmPackage;
import no.ntnu.tdt4250.sm.State;
import no.ntnu.tdt4250.sm.StateMachine;
import no.ntnu.tdt4250.sm.Transition;

public class StateMachineExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		SmPackage p = SmPackage.eINSTANCE;
//		SmFactory f = p.getSmFactory();
		
		SmFactory factory = SmPackage.eINSTANCE.getSmFactory();
		
		State s1 = factory.createState();
		s1.setName("s1");
		
		State s2 = factory.createState();
		s2.setName("s2");
		
		Transition t = factory.createTransition();
		t.setSource(s1);
		t.setTarget(s2);
		
		StateMachine sm = factory.createStateMachine();
		sm.getState().add(s1);
		sm.getState().add(s2);
		sm.getTransition().add(t);
		sm.setInitial(s1);
		
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("sm", new XMIResourceFactoryImpl());

        // Obtain a new resource set
        ResourceSet resSet = new ResourceSetImpl();

        // create a resource
        Resource resource = resSet.createResource(URI
                .createURI("ExampleSM.sm"));
        // Get the first model element and cast it to the right type, in my
        // example everything is hierarchical included in this first node
        resource.getContents().add(sm);

        try {
			resource.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
