/**
 */
package no.ntnu.tdt4250.sm.util;

import java.util.Map;

import no.ntnu.tdt4250.sm.*;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see no.ntnu.tdt4250.sm.SmPackage
 * @generated
 */
public class SmValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final SmValidator INSTANCE = new SmValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "no.ntnu.tdt4250.sm";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Initial States Should Not Have Incoming Transitions' of 'State Machine'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int STATE_MACHINE__INITIAL_STATES_SHOULD_NOT_HAVE_INCOMING_TRANSITIONS = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Final States Should Not Have Outgoing Transitions' of 'State Machine'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int STATE_MACHINE__FINAL_STATES_SHOULD_NOT_HAVE_OUTGOING_TRANSITIONS = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return SmPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case SmPackage.STATE:
			return validateState((State) value, diagnostics, context);
		case SmPackage.TRANSITION:
			return validateTransition((Transition) value, diagnostics, context);
		case SmPackage.STATE_MACHINE:
			return validateStateMachine((StateMachine) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateState(State state, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(state, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateMachine(StateMachine stateMachine, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(stateMachine, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(stateMachine, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateStateMachine_finalStatesShouldNotHaveOutgoingTransitions(stateMachine, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validateStateMachine_initialStatesShouldNotHaveIncomingTransitions(stateMachine, diagnostics,
					context);
		return result;
	}

	/**
	 * Validates the finalStatesShouldNotHaveOutgoingTransitions constraint of '<em>State Machine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateMachine_finalStatesShouldNotHaveOutgoingTransitions(StateMachine stateMachine,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return stateMachine.finalStatesShouldNotHaveOutgoingTransitions(diagnostics, context);
	}

	/**
	 * Validates the initialStatesShouldNotHaveIncomingTransitions constraint of '<em>State Machine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateMachine_initialStatesShouldNotHaveIncomingTransitions(StateMachine stateMachine,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return stateMachine.initialStatesShouldNotHaveIncomingTransitions(diagnostics, context);
	}

	//	/**
	//	 * Validates the finalStatesShouldNotHaveOutgoingTransitions constraint of '<em>State Machine</em>'.
	//	 * <!-- begin-user-doc -->
	//	 * <!-- end-user-doc -->
	//	 * @generated NOT
	//	 */
	//	public boolean validateStateMachine_finalStatesShouldNotHaveOutgoingTransitions(StateMachine stateMachine,
	//			DiagnosticChain diagnostics, Map<Object, Object> context) {
	//		// TODO implement the constraint
	//		// -> specify the condition that violates the constraint
	//		// -> verify the diagnostic details, including severity, code, and message
	//		// Ensure that you remove @generated or mark it @generated NOT
	//		boolean valid = true;
	//		
	//		for (State s : stateMachine.getFinal()) {
	//			if(s.getOutgoing().size() > 0) {
	//				valid = false;
	//			}
	//		}
	//		
	//		if (!valid) {
	//			if (diagnostics != null) {
	//				diagnostics.add(createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
	//						"_UI_GenericConstraint_diagnostic", new Object[] {
	//								"finalStatesShouldNotHaveOutgoingTransitions", getObjectLabel(stateMachine, context) },
	//						new Object[] { stateMachine }, context));
	//			}
	//			return false;
	//		}
	//		return true;
	//	}

	//	/**
	//	 * Validates the initialStatesShouldNotHaveIncomingTransitions constraint of '<em>State Machine</em>'.
	//	 * <!-- begin-user-doc -->
	//	 * <!-- end-user-doc -->
	//	 * @generated NOT
	//	 */
	//	public boolean validateStateMachine_initialStatesShouldNotHaveIncomingTransitions(StateMachine stateMachine,
	//			DiagnosticChain diagnostics, Map<Object, Object> context) {
	//		// TODO implement the constraint
	//		// -> specify the condition that violates the constraint
	//		// -> verify the diagnostic details, including severity, code, and message
	//		// Ensure that you remove @generated or mark it @generated NOT
	//		boolean valid = true;
	//
	//		if (stateMachine.getInitial().getIncoming().size() > 0) {
	//			valid = false;
	//		}
	//
	//		if (valid) {
	//			if (diagnostics != null) {
	//				diagnostics.add(
	//						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0, "_UI_GenericConstraint_diagnostic",
	//								new Object[] { "initialStatesShouldNotHaveIncomingTransitions",
	//										getObjectLabel(stateMachine, context) },
	//								new Object[] { stateMachine }, context));
	//			}
	//			return false;
	//		}
	//		return true;
	//	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //SmValidator
