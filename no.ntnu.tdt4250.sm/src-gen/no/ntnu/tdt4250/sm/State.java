/**
 */
package no.ntnu.tdt4250.sm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.sm.State#getName <em>Name</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#getChildren <em>Children</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#isInitial <em>Initial</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.State#isHasLoops <em>Has Loops</em>}</li>
 * </ul>
 *
 * @see no.ntnu.tdt4250.sm.SmPackage#getState()
 * @model
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.sm.State#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.State}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getChildren();

	/**
	 * Returns the value of the '<em><b>Initial</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' attribute.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Initial()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isInitial();

	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.Transition}.
	 * It is bidirectional and its opposite is '{@link no.ntnu.tdt4250.sm.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Outgoing()
	 * @see no.ntnu.tdt4250.sm.Transition#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Transition> getOutgoing();

	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.Transition}.
	 * It is bidirectional and its opposite is '{@link no.ntnu.tdt4250.sm.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_Incoming()
	 * @see no.ntnu.tdt4250.sm.Transition#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<Transition> getIncoming();

	/**
	 * Returns the value of the '<em><b>Has Loops</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Loops</em>' attribute.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getState_HasLoops()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isHasLoops();

} // State
