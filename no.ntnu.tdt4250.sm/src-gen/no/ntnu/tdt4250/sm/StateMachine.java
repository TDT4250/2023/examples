/**
 */
package no.ntnu.tdt4250.sm;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getState <em>State</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getTransition <em>Transition</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getName <em>Name</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getInitial <em>Initial</em>}</li>
 *   <li>{@link no.ntnu.tdt4250.sm.StateMachine#getFinal <em>Final</em>}</li>
 * </ul>
 *
 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='finalStatesShouldNotHaveOutgoingTransitions'"
 * @generated
 */
public interface StateMachine extends EObject {
	/**
	 * Returns the value of the '<em><b>State</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.State}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' containment reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_State()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<State> getState();

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' containment reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.Transition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' containment reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_Transition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Transition> getTransition();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.sm.StateMachine#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Initial</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' reference.
	 * @see #setInitial(State)
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_Initial()
	 * @model resolveProxies="false" required="true"
	 * @generated
	 */
	State getInitial();

	/**
	 * Sets the value of the '{@link no.ntnu.tdt4250.sm.StateMachine#getInitial <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial</em>' reference.
	 * @see #getInitial()
	 * @generated
	 */
	void setInitial(State value);

	/**
	 * Returns the value of the '<em><b>Final</b></em>' reference list.
	 * The list contents are of type {@link no.ntnu.tdt4250.sm.State}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final</em>' reference list.
	 * @see no.ntnu.tdt4250.sm.SmPackage#getStateMachine_Final()
	 * @model required="true"
	 * @generated
	 */
	EList<State> getFinal();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='self.initial.incoming-&gt;size() = 0'"
	 * @generated
	 */
	boolean initialStatesShouldNotHaveIncomingTransitions(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='self.final-&gt;forAll(outgoing-&gt;size() = 0)'"
	 * @generated
	 */
	boolean finalStatesShouldNotHaveOutgoingTransitions(DiagnosticChain diagnostics, Map<Object, Object> context);

} // StateMachine
