package no.ntnu.tdt4250.sm.examples.standalone;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emfcloud.jackson.resource.JsonResourceFactory;

import no.ntnu.tdt4250.sm.SmFactory;
import no.ntnu.tdt4250.sm.SmPackage;
import no.ntnu.tdt4250.sm.State;
import no.ntnu.tdt4250.sm.StateMachine;
import no.ntnu.tdt4250.sm.Transition;
import no.ntnu.tdt4250.sm.impl.SmPackageImpl;


public class ExampleStandalone {

	public static void main(String[] args) {
	
		/* ====================================
		 *  BOILERPLATE INITIALIZATION OF EMF
		 *  REQUIRED WHEN RUNNING STANDALONE 
		 * ==================================== */
		
		// Initialization of package and factory - need to create model objects
		// The name of these classes depend on the metamodel name - Here "Sm" because the metamodel has such name.
		SmPackage myPackage = SmPackageImpl.eINSTANCE;
		SmFactory myFactory = myPackage.getSmFactory();
		
		// Adding the metamodel to the EMF metamodel registry
		// (the pair "namespace/package instance" is added to a map)
		EPackage.Registry.INSTANCE.put(myPackage.getNsURI(), myPackage);
		
		// Registration of the XMI resource factory
		// Defines that models (files) with extension ".sm" should be de-serialized as XMI
		// Potentially, different implementation of resource factory can be used or created
		// Here the pair "extension/instance of resource factory" is also added to a map
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("sm", new XMIResourceFactoryImpl());
        
        // Registering the JSON resource factory for .smj extension
        // This requires the following dependencies:
        // - org.eclipse.emfcloud.emfjson-jackson
        // - com.fasterxml.jackson.core.jackson-annotations
        // - com.fasterxml.jackson.core.jackson-core
        // - com.fasterxml.jackson.core.jackson-databind
        m.put("smj", new JsonResourceFactory());

        
        /* =================================================
         *  READING AN ECORE RESOURCE (MODEL IN XMI FORMAT) 
         * ================================================= */ 
        
        // Obtain a new resource set
        ResourceSet resSet = new ResourceSetImpl();

        // Load a resource (replace the path with the correct one)
        // Relative paths are relative to the project root folder.
        URI uri = URI.createURI("/home/montex/git/examples/no.ntnu.tdt4250.sm.examples/Oven.sm");
        System.out.println("> Loading: " + uri);
        Resource resource = resSet.getResource(uri, true);
        System.out.println("> Loaded.");
        

        // Get the first model element and cast it to the right type
        // The "contents" property of a resource returns the top-level elements contained in it.
        // In our case an instance of the StateMachine metaclass. The object is returned as an EObject.s 

        StateMachine sm = (StateMachine)resource.getContents().get(0);
        
        // Print some data from the model and iterate over children elements
        System.out.println("This state machine has " + sm.getState().size() + " states:");

        String temp;
        for (State s : sm.getState()) {
			temp = "- " + s.getName();
			if(s.isInitial()) {
				temp += " [Initial]";
			}
			if(sm.getFinal().contains(s)) {
				temp += " [Final]";
			}
			System.out.println(temp);
		}

		System.out.println("===========================================");    	
        
        /* ===========================================
         *  CREATING AN ECORE MODEL PROGRAMMATICALLY 
         * =========================================== */ 
        
        // Create a new StateMachine - we must use the factory
        
        StateMachine anotherSM = myFactory.createStateMachine();
        anotherSM.setName("EspressoMachine");
        
        State[] states = new State[4];
        String[] labels = { "Off", "Idle", "Grinding", "Brewing" };
        
        State tmpState;
        for (int i = 0; i < states.length; i++) {
			states[i] = myFactory.createState();
			states[i].setName(labels[i]);
			// Adding the new state to the state machine
			anotherSM.getState().add(states[i]);
		}
        // In this state machine we have two final states
        anotherSM.getFinal().add(states[0]);
        anotherSM.getFinal().add(states[1]);

        Transition tPowerOn = myFactory.createTransition();
        tPowerOn.setName("PowerOn");
        tPowerOn.setSource(states[0]);
        tPowerOn.setTarget(states[1]);
        
        Transition tGrind = myFactory.createTransition();
        tGrind.setName("Grind");
        tGrind.setSource(states[1]);
        tGrind.setTarget(states[2]);
        
        Transition tBrew = myFactory.createTransition();
        tBrew.setName("Brew");
        tBrew.setSource(states[2]);
        tBrew.setTarget(states[3]);
        
        Transition tFinish = myFactory.createTransition();
        tFinish.setName("Finish");
        tFinish.setSource(states[3]);
        tFinish.setTarget(states[1]);
        
        // Just using a list instead of adding one by one
    	anotherSM.getTransition().addAll(
    			List.of(tPowerOn, tGrind, tBrew, tFinish)
    	);
    	
    	// Creating a new resource (xmi file) to store the model
    	// (note that getResource is replaced by createResource)
    	// 
    	// We use in this case a relative path
    	// The file should be created in the same project - press F5 to refresh
    	Resource anotherResource = resSet.createResource(URI.createURI("Espresso.sm"));
    	anotherResource.getContents().add(anotherSM);
    	
    	// Save resource to an xmi file
    	// Need to surround with try/catch because of possible IOException
    	// The "null" parameter can be a map with save options
    	try {
    		System.out.println("> Saving: " + anotherResource.getURI());
			anotherResource.save(null);
			System.out.println("> Saved.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		System.out.println("===========================================");    	
       
        /* ===========================================
         *  TRANSFORMATION EXAMPLE
         * =========================================== */ 

    	// Example of model transformation. We take the Oven state machine, add a 
    	// self-loop for every state, and save it as a new file.
    	
    	// For each state in the "Oven" state machine (it was loaded earlier)
    	Transition tempTransition;
    	for (State theState : sm.getState()) {
			tempTransition = myFactory.createTransition();
			// It's a loop, so source and target are the same state
			tempTransition.setSource(theState);
			tempTransition.setTarget(theState);
			tempTransition.setName(theState.getName() + "Loop");
			// Add the new transition to the state machine
			sm.getTransition().add(tempTransition);
		}
    	
    	Resource ovenModified = resSet.createResource(URI.createURI("Oven-Transformed.sm"));
    	ovenModified.getContents().add(sm);
    	
    	try {
    		// Using an empty map now, just to show how one would add options
    		Map<?,?> options = new HashMap<>();
    		System.out.println("> Saving: " + ovenModified.getURI());
			ovenModified.save(options);
			System.out.println("> Saved.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Saving the same model also in JSON format
		// (Refer to the registration of .smj extension earlier in this same file)
		Resource ovenModifiedJson = resSet.createResource(URI.createURI("Oven-Transformed.smj"));
		ovenModifiedJson.getContents().add(sm);

		try {
			System.out.println("> Saving: " + ovenModifiedJson.getURI());
			ovenModifiedJson.save(null);
			System.out.println("> Saved.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
