package no.ntnu.tdt4250.sm.genjavaxtend

import no.ntnu.tdt4250.sm.StateMachine
import no.ntnu.tdt4250.sm.State

class StateMachineGenerator {

	val StateMachine machine;
	
	new(StateMachine sm) {
		machine = sm;
	}
		
	def generateMachine()'''
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class «machine.name» {

	public static void main(String[] args) {
	

		try {
			new «machine.name»().run();
		} catch (IOException e) {
			System.out.println("Exception occurred, exiting!");
			e.printStackTrace();
		}
	}

	private void run() throws IOException {
		MachineState current = «machine.initial.enumItem(true)»;
		
		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
		String input;

		while(«finalCondition») {
			switch(current) {
			
			«FOR s : machine.state»
				case «s.enumItem()»:
					System.out.println("We are now in state «s.name», welcome!");
					System.out.println("Please select a transition:");
					«FOR t : s.outgoing»
					System.out.println("- «t.name»");
					«ENDFOR»
					input = buffer.readLine();
					System.out.println("You selected: " + input);
					
					switch(input) {
						«FOR t : s.outgoing»
						case "«t.name»":
							current = «t.target.enumItem(true)»;
							break;
						«ENDFOR»
						default:
							System.out.println("Invalid transition, sorry");
							break;		
					}
				break;

			«ENDFOR»
		  	}
		}
		
		System.out.println("Machine ended.")
    }
}
'''

	def generateMachineStates()'''
	public enum MachineState {
		«FOR s : machine.state SEPARATOR ","»
		«s.enumItem»
		«ENDFOR»
	}
'''
		
	def private finalCondition() {
		// In this function we are using anonymous functions and map
		// (see Java streams API and functional programming)
		// We could have done the same with a for loop (like above)
		
		machine.final.map[
			'''current != «it.enumItem(true)»'''
		].join(" && ")	
	}


	def private enumItem(State s, boolean full) {
		var ret = "STATE_" + s.name.toUpperCase
		
		if(full) ret = "MachineState." + ret
		
		ret
	}
	
	def private enumItem(State s) {
		enumItem(s, false);
	}
	

}