package no.ntnu.tdt4250.sm.genjavaxtend;

import java.io.FileWriter;
import java.util.Map;
import no.ntnu.tdt4250.sm.SmPackage;
import no.ntnu.tdt4250.sm.StateMachine;
import no.ntnu.tdt4250.sm.impl.SmPackageImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class GeneratorRunner {
  public static void main(final String[] args) {
    final String model = "/home/montex/TDT4250/examples/no.ntnu.tdt4250.sm/ExampleSM.sm";
    final String folder = "/home/montex/TDT4250/workspace-2/no.ntnu.tdt4250.sm.genjavaxtend/src";
    GeneratorRunner.registerMetamodel();
    GeneratorRunner.registerResourceFactory();
    final StateMachine machine = GeneratorRunner.loadModel(model);
    final StateMachineGenerator generator = new StateMachineGenerator(machine);
    String _string = generator.generateMachine().toString();
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(folder);
    _builder.append("/");
    String _name = machine.getName();
    _builder.append(_name);
    _builder.append(".java");
    GeneratorRunner.writeText(_string, _builder.toString());
    String _string_1 = generator.generateMachineStates().toString();
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append(folder);
    _builder_1.append("/MachineState.java");
    GeneratorRunner.writeText(_string_1, _builder_1.toString());
  }

  public static String writeText(final String text, final String filePath) {
    try {
      String _xblockexpression = null;
      {
        InputOutput.<String>println(("> Writing: " + filePath));
        final FileWriter file = new FileWriter(filePath, false);
        file.write(text);
        file.close();
        _xblockexpression = InputOutput.<String>println("> Done.");
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  public static StateMachine loadModel(final String filePath) {
    StateMachine _xblockexpression = null;
    {
      final ResourceSet resSet = new ResourceSetImpl();
      final URI uri = URI.createURI(filePath);
      InputOutput.<String>println(("> Loading: " + uri));
      final Resource resource = resSet.getResource(uri, true);
      InputOutput.<String>println("> Loaded.");
      EObject _head = IterableExtensions.<EObject>head(resource.getContents());
      _xblockexpression = ((StateMachine) _head);
    }
    return _xblockexpression;
  }

  public static void registerMetamodel() {
    final SmPackage myPackage = SmPackageImpl.eINSTANCE;
    EPackage.Registry.INSTANCE.put(myPackage.getNsURI(), myPackage);
  }

  public static Object registerResourceFactory() {
    Object _xblockexpression = null;
    {
      final Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
      final Map<String, Object> m = reg.getExtensionToFactoryMap();
      XMIResourceFactoryImpl _xMIResourceFactoryImpl = new XMIResourceFactoryImpl();
      _xblockexpression = m.put("sm", _xMIResourceFactoryImpl);
    }
    return _xblockexpression;
  }
}
