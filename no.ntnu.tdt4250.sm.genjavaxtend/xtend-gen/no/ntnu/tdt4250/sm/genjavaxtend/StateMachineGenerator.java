package no.ntnu.tdt4250.sm.genjavaxtend;

import no.ntnu.tdt4250.sm.State;
import no.ntnu.tdt4250.sm.StateMachine;
import no.ntnu.tdt4250.sm.Transition;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class StateMachineGenerator {
  private final StateMachine machine;

  public StateMachineGenerator(final StateMachine sm) {
    this.machine = sm;
  }

  public CharSequence generateMachine() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import java.io.BufferedReader;");
    _builder.newLine();
    _builder.append("import java.io.InputStreamReader;");
    _builder.newLine();
    _builder.append("import java.io.IOException;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class ");
    String _name = this.machine.getName();
    _builder.append(_name);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static void main(String[] args) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("new ");
    String _name_1 = this.machine.getName();
    _builder.append(_name_1, "\t\t\t");
    _builder.append("().run();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("} catch (IOException e) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("System.out.println(\"Exception occurred, exiting!\");");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("e.printStackTrace();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private void run() throws IOException {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("MachineState current = ");
    String _enumItem = this.enumItem(this.machine.getInitial(), true);
    _builder.append(_enumItem, "\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String input;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("while(");
    String _finalCondition = this.finalCondition();
    _builder.append(_finalCondition, "\t\t");
    _builder.append(") {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("switch(current) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.newLine();
    {
      EList<State> _state = this.machine.getState();
      for(final State s : _state) {
        _builder.append("\t\t\t");
        _builder.append("case ");
        String _enumItem_1 = this.enumItem(s);
        _builder.append(_enumItem_1, "\t\t\t");
        _builder.append(":");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t");
        _builder.append("\t");
        _builder.append("System.out.println(\"We are now in state ");
        String _name_2 = s.getName();
        _builder.append(_name_2, "\t\t\t\t");
        _builder.append(", welcome!\");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t");
        _builder.append("\t");
        _builder.append("System.out.println(\"Please select a transition:\");");
        _builder.newLine();
        {
          EList<Transition> _outgoing = s.getOutgoing();
          for(final Transition t : _outgoing) {
            _builder.append("\t\t\t");
            _builder.append("\t");
            _builder.append("System.out.println(\"- ");
            String _name_3 = t.getName();
            _builder.append(_name_3, "\t\t\t\t");
            _builder.append("\");");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t\t\t");
        _builder.append("\t");
        _builder.append("input = buffer.readLine();");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("\t");
        _builder.append("System.out.println(\"You selected: \" + input);");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("\t");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("\t");
        _builder.append("switch(input) {");
        _builder.newLine();
        {
          EList<Transition> _outgoing_1 = s.getOutgoing();
          for(final Transition t_1 : _outgoing_1) {
            _builder.append("\t\t\t");
            _builder.append("\t\t");
            _builder.append("case \"");
            String _name_4 = t_1.getName();
            _builder.append(_name_4, "\t\t\t\t\t");
            _builder.append("\":");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t\t");
            _builder.append("\t\t");
            _builder.append("\t");
            _builder.append("current = ");
            String _enumItem_2 = this.enumItem(t_1.getTarget(), true);
            _builder.append(_enumItem_2, "\t\t\t\t\t\t");
            _builder.append(";");
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t\t");
            _builder.append("\t\t");
            _builder.append("\t");
            _builder.append("break;");
            _builder.newLine();
          }
        }
        _builder.append("\t\t\t");
        _builder.append("\t\t");
        _builder.append("default:");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("\t\t\t");
        _builder.append("System.out.println(\"Invalid transition, sorry\");");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("\t\t\t");
        _builder.append("break;\t\t");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("\t");
        _builder.append("}");
        _builder.newLine();
        _builder.append("\t\t\t");
        _builder.append("break;");
        _builder.newLine();
        _builder.newLine();
      }
    }
    _builder.append("\t\t  \t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("System.out.println(\"Machine ended.\")");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }

  public CharSequence generateMachineStates() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("public enum MachineState {");
    _builder.newLine();
    {
      EList<State> _state = this.machine.getState();
      boolean _hasElements = false;
      for(final State s : _state) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "\t");
        }
        _builder.append("\t");
        String _enumItem = this.enumItem(s);
        _builder.append(_enumItem, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }

  private String finalCondition() {
    final Function1<State, String> _function = (State it) -> {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("current != ");
      String _enumItem = this.enumItem(it, true);
      _builder.append(_enumItem);
      return _builder.toString();
    };
    return IterableExtensions.join(ListExtensions.<State, String>map(this.machine.getFinal(), _function), " && ");
  }

  private String enumItem(final State s, final boolean full) {
    String _xblockexpression = null;
    {
      String _upperCase = s.getName().toUpperCase();
      String ret = ("STATE_" + _upperCase);
      if (full) {
        ret = ("MachineState." + ret);
      }
      _xblockexpression = ret;
    }
    return _xblockexpression;
  }

  private String enumItem(final State s) {
    return this.enumItem(s, false);
  }
}
